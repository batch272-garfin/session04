
-- Adding New Records

-- 5 Artists
INSERT INTO artists (name) VALUES
("Taylor Swift"),
("Lady Gaga"),
("Jihoon"),
("Ariana Grande"),
("Bruno Mars");

-- 2 Albums for Taylor Swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Fearless", "2008-1-1", 3),
("Red", "2014-1-1", 3);

-- 2 Songs for Fearless Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Fearless", 314, "Pop Rock", 3),
("State of Grace", 421, "Rock", 3);

-- 2 Songs for Red Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Red", 312, "Pop", 4),
("Begin Again", 441, "Country", 4);

-- 2 Albums for Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("A Star is Born", "2020-1-1", 4),
("Born This Way", "2012-1-1", 4);

-- 2 Songs for A Star is Born Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Shallow", 441, "Country Rock", 5),
("Always Remember Us This Way", 325, "Country", 5);

-- 2 Songs for Born this Way
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Bad Romance", 352, "Pop", 6),
("Paparazzi", 225, "Pop", 6);

-- 2 Albums for Jihoon
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Oclock", "2019-1-1", 4),
("The Answer", "2012-1-1", 4);

-- 1 Song for Oclock Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("L.O.V.E", 441, "K-pop", 7);

-- 1 Song for The Answer Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Nitro", 352, "K-pop", 8);

-- 1 Song for Oclock Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Moon", 441, "K-pop", 7);

-- 1 Song for The Answer Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Talk to Me", 352, "K-pop", 8);

-- 2 Albums for Ariana grande
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Dangerous Woman", "2016-1-1", 6),
("Thank You! Next", "2019-1-1", 6);

-- 1 Song for Dangerous Woman Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Into You", 341, "EDM House", 9);

-- 1 Song for Thank You! Next Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Thank You! Next", 426, "Pop, R&B", 10);

-- 2 Albums for Bruno Mars
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("24K Magic", "2016-1-1", 7),
("Earth to Mars", "2011-1-1", 7);

-- 1 Song for 24K MAgic Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("24K Magic", 401, "Funk, Disco, R&B", 11);

-- 1 Song for Earth to Mars Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Lost", 356, "Pop", 12);


-- Advanced Selection

-- Getting song with an id of 11
SELECT * FROM songs WHERE id = 11;

-- Getting all the songs with an id except for 11.
SELECT * FROM songs WHERE id != 11;

-- Less than or equal
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id <= 11;

-- Greater than or equal
SELECT * FROM songs WHERE id > 11;
SELECT * FROM songs WHERE id >= 11;

-- Get specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

-- Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 3, 7);
SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");

-- Combining Conditions
SELECT * FROM songs WHERE album_id = 4 AND id > 15; 

-- Find Partial Matches (not case-sensitive)
-- %_ for Last Letter
SELECT * FROM songs WHERE song_name LIKE "%n";

-- _% for First Letter
SELECT * FROM songs WHERE song_name LIKE "m%";

-- %_% for Middle Letter
SELECT * FROM songs WHERE song_name LIKE "%o%";

-- Sorting Keywords
-- Ascending Order
SELECT * FROM songs ORDER BY song_name ASC;

-- Descending Order
SELECT * FROM songs ORDER BY song_name DESC;

-- Getting Distinct Records
SELECT DISTINCT genre FROM song_name;


[3] Table Joins

-- Combine artists table and albums table
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- More than 2 tables
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- More than 2 tables with condition
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id WHERE name = "Taylor Swift";

-- Getting columns from 2 tables

SELECT artists.name, albums.album_title
FROM artists
JOIN albums ON artists.id = albums.artist_id;

-- Getting columns from 3 tables
SELECT artists.name, albums.album_title, songs.song_name
FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.artist_id = songs.album_id;

-- Show artists without records on the right side

SELECT * FROM artists
LEFT JOIN albums ON artists.id = albums.artist_id;